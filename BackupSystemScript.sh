#!/bin/bash

#***********************************************************************
#***********************BACKUP SYSTEM SCRIPT****************************
#*****creation_date:08/12/2020--------------------------Author:R3no*****
#***********************************************************************

#*******************************FUNZIONI********************************

timestamp() {
    echo $(date +'%Y-%m-%d %H:%M:%S') # current time
}

gestioneLog() {
    LOG_FILE_DATA=$(date +"%Y%m%d_%H%M%S")
    LOG_FILE="BackupSystemScript-$LOG_FILE_DATA.log"
    #LOG_FILE="main_backup_home-TEST.log"

    #
    #Se non esiste creo la directory logs
    #
    if [ -d "$ROOT"/LogFiles ]; then
        #Esporto il file di log
        export logFile="$ROOT"/LogFiles/$LOG_FILE
    else
        mkdir "$ROOT"/LogFiles
        export logFile="$ROOT"/LogFiles/$LOG_FILE
    fi
    #
    #Intestazione del file
    #
    echo -e "***********************************************************************" >${logFile}
    echo -e "***********************BACKUP HOME DIRECTORY***************************" >>${logFile}
    echo -e "*****data:08/12/2020-----------------------------------Author:Rino*****" >>${logFile}
    echo -e "***********************************************************************" >>${logFile}
}

startMain() {

    #*****STEP_0
    if [[ "$isInError" == 0 && "$sdaBackup" == "y" ]]; then
        STEP_0=0
        step="*****STEP_0"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Copio il disco sda 1, 3 in un bz2..." >>${logFile}
        #Se presente elimino il vecchio file
        echo -e "\n$(timestamp) ******Elimino il vecchio file..." >>${logFile}
        for i in 1 3; do
            STEP_0=0
            if [ -f /media/veracrypt1/BackupSystem/PopLinux/poplinux_rinok550_500gb_sda$i.bz2 ]; then
                rm -rf /media/veracrypt1/BackupSystem/PopLinux/poplinux_rinok550_500gb_sda$i.bz2 &
            fi
            for job in $(jobs -p); do
                wait $job || let "STEP_0+=1"
            done
            if [[ "$STEP_0" == 0 ]]; then
                echo -e "\n$(timestamp) Vecchio file sda$i eliminato correttamente..." >>${logFile}
            else
                isInError=1
            fi
            #Inizio la copia del nuovo file
            if [[ "$isInError" == 0 ]]; then
                STEP_0=0
                #Con tutti i progressi
                #sudo dd if=/dev/sda status=progress 2>${logFile} | bzip2 >/media/veracrypt1/BackupSystem/PopLinux/poplinux_rinok550_500gb_sda.bz2 &
                #Con progressi solo alla fine
                sudo dd if=/dev/sda$i 2>>${logFile} | bzip2 >/media/veracrypt1/BackupSystem/PopLinux/poplinux_rinok550_500gb_sda$i.bz2 &
                for job in $(jobs -p); do
                    wait $job || let "STEP_0+=1"
                done
                if [[ "$STEP_0" == 0 ]]; then
                    echo -e "\n$(timestamp) Disco sda$i copiato correttamente..." >>${logFile}
                else
                    echo -e "\n$(timestamp) Disco sda$i copiato con errore..." >>${logFile}
                    isInError=1
                fi
            else
                echo -e "\n$(timestamp) Vecchio file sda$i eliminato con errore..." >>${logFile}
            fi
        done
    else
        echo -e "\n$(timestamp) ******Hai scelto di non fare il backup di sda 1,3...skip!" >>${logFile}
    fi

    #*****STEP_1
    if [[ "$isInError" == 0 ]]; then
        STEP_1=0
        step="*****STEP_1"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Smonto tutti i dischi montati..." >>${logFile}
    #Smonto tutti i dischi
        veracrypt -f -d
        for job in $(jobs -p); do
            wait $job || let "STEP_1+=1"
        done
        if [[ "$STEP_1" == 0 ]]; then
            echo -e "\n$(timestamp) Tutti i dischi montati sono stati smontati correttamente..." >>${logFile}
        else
            echo -e "\n$(timestamp) Tutti i dischi montati NON sono stati smontati correttamente..." >>${logFile}
        fi
    fi

    #*****STEP_2
    if [[ "$isInError" == 0 ]]; then
        STEP_2=0
        step="*****STEP_2"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Decripto l'hdd portatile..." >>${logFile}
        veracrypt -t /dev/disk/by-partuuid/336ac436-01 --password="$veraPassword" --slot=1 --non-interactive > >(tee -a ${logFile} >/dev/null) &
            #Check del processo
            for job in $(jobs -p); do
                wait $job || let "STEP_2+=1"
            done
            if [[ "$STEP_2" == 0 ]]; then
                echo -e "\n$(timestamp) Hdd portatile decriptato con sucesso..." >>${logFile}
            else
                echo -e "\n$(timestamp) Hdd portatile decriptato con errore..." >>${logFile}
                isInError=1
            fi
    fi

    #*****STEP_3
    if [[ "$isInError" == 0 ]]; then
        STEP_3=0
        step="*****STEP_3"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Copio la home del notebook su hdd portatile..." >>${logFile}
        sudo rsync --progress --stats -avz --delete --exclude='found.*' --exclude='$RECYCLE.BIN' --exclude='System Volume Information' --exclude='*.tmp' --exclude='Thumbs.db' --exclude='.Trash*' --exclude='*~*' --exclude='.cache' /home /media/veracrypt1 2>&1 &
        # > >(tee -a ${logFile} >/dev/null) &
        sleep 3
        for job in $(jobs -p); do
            wait $job || let "STEP_3+=1"
        done
        if [[ "$STEP_3" == 0 ]]; then
            echo -e "\n$(timestamp) Copia di notebook home su hdd portatile avvenuta con successo..." >>${logFile}
        else
            echo -e "\n$(timestamp) Copia di notebook home su hdd portatile avvenuta con errore..." >>${logFile}
            isInError=1
        fi
    fi

    #*****STEP_4
    if [[ "$isInError" == 0 ]]; then
        STEP_4=0
        step="*****STEP_4"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Decripto l'hdd fisso..." >>${logFile}
        veracrypt -t /dev/disk/by-partuuid/12c2954c-01 --password="$veraPassword" --slot=2 --non-interactive > >(tee -a ${logFile} >/dev/null) &
            #Check del processo
            for job in $(jobs -p); do
                wait $job || let "STEP_4+=1"
            done
            if [[ "$STEP_4" == 0 ]]; then
                echo -e "\n$(timestamp) Hdd fisso decriptato con sucesso..." >>${logFile}
            else
                echo -e "\n$(timestamp) Hdd fisso decriptato con errore..." >>${logFile}
                isInError=1
            fi
    fi

    #*****STEP_5
    if [[ "$isInError" == 0 ]]; then
        STEP_5=0
        step="*****STEP_5"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Copio l' hdd portatile su hdd fisso..." >>${logFile}
        sudo rsync --progress --stats -avz --delete --exclude='found.*' --exclude='$RECYCLE.BIN' --exclude='System Volume Information' --exclude='*.tmp' --exclude='Thumbs.db' --exclude='.Trash*' --exclude='*~*' --exclude='.cache' /media/veracrypt1/ /media/veracrypt2/ 2>&1 &
        #> >(tee -a ${logFile} >/dev/null) &
        for job in $(jobs -p); do
            wait $job || let "STEP_5+=1"
        done
        if [[ "$STEP_5" == 0 ]]; then
            echo -e "\n$(timestamp) Copia da hdd portatile su hdd fisso avvenuta con successo..." >>${logFile}
        else
            echo -e "\n$(timestamp) Copia da hdd portatile su hdd fisso avvenuta con errore..." >>${logFile}
            isInError=1
        fi
    fi

    #*****STEP_6
    if [[ "$isInError" == 0 ]]; then
        STEP_6=0
        step="*****STEP_6"
        echo -e "\n$(timestamp) $step" >>${logFile}
        echo -e "\n$(timestamp) ******Smonto tutti i dischi montati..." >>${logFile}
        #Smonto tutti i dischi
        veracrypt /f /d
        for job in $(jobs -p); do
            wait $job || let "STEP_6+=1"
        done
        if [[ "$STEP_6" == 0 ]]; then
            echo -e "\n$(timestamp) Disco smontati correttamente..." >>${logFile}
        else
            echo -e "\n$(timestamp) Dischi smontati, tentativo con errore..." >>${logFile}
            isInError=1
        fi
    fi

    #*****END_MAIN
    if [[ "$isInError" == 0 ]]; then
        echo -e "\n$(timestamp) Processo terminato con successo..." >>${logFile}
        msg="Processo terminato con successo..."
    else
        echo -e "\n$(timestamp) Processo terminato con errore allo step $step">>${logFile}
        msg="Processo terminato con errore allo step $step"
    fi

    #bash -lic "paplay /usr/share/sounds/gnome/default/alerts/bark.ogg"
    #bash -lic "spd-say 'Copia terminata     strunz!'"
    zenity --info --no-wrap --text="${msg}"

}

#*******************************START***********************************

#Clear
clear

#ROOT GENERALE
export ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
isInError=0
sdaBackup=""
step=""

#Esporto la password
source "$ROOT"/Utility/password.sh

#Preparo i log
gestioneLog

#Scelta per capire se devo eseguire il backup del disco rigido
while [[ true ]]; do
    clear
    echo -e "Vuoi eseguire un nuovo backup di sda 1, 3? \n(y=yes; n=no; exit per exit) :"
    read sdaBackup
    if [[ "$sdaBackup" == "y" || "$sdaBackup" == "n" ]]; then
        break
    elif [[ "$sdaBackup" == "exit" ]]; then
        clear
        exit 0
    fi
done

#Eseguo il main
startMain &
